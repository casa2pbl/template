<?php

	class Candidato {
		var $nome;
		var $apelido;
		var $ano_nasc;

		/* metodo construtor */
		function __construct($nome,$apelido,$ano_nasc)
		{
			$this->nome = $nome;
			$this->apelido = $apelido;
			$this->ano_nasc = $ano_nasc;
		}

		function setNome($nome){
			$this->nome = $nome;
		}

		function setApelido($apelido){
			$this->apelido=$apelido;
		}

		function setAno_Nasc($ano_nasc){
			$this->ano_nasc=$ano_nasc;
		}

		function getNome(){
			return $this->nome;
		}

		function getApelidot(){
			return $this->apelido;
		}

		function getAno_Nasc(){
			return $this->ano_nasc;
		}
	}

?>