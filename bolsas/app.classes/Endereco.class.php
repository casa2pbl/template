<?php
	/**
	* 
	*/
	class Endereco
	{	
		var $nacionalidade;
		var $provincia;
		var $cidade;
		var $bairro;
		var $avenida;
		var $quarteirao;
		var $num_casa;

		function __construct($nacionalidade,$provincia,$telefone,$email,$cidade,
			$bairro,$avenida,$quarteirao,$num_casa)
		{
			$this->nacionalidade = $nacionalidade;
			$this->provincia = $provincia;
			$this->bairro = $bairro;
			$this->avenida = $avenida;
			$this->quarteirao = $quarteirao;
			$this->num_casa = $num_casa;
		}

		function setNacionalidade($nacionalidade){
			$this->nacionalidade=$nacionalidade;
		}

		function setProvincia($provincia){
			$this ->provincia = $provincia;
		}

		function setBairro($bairro){
			$this->bairro = $bairro;
		}

		function setAvenida($avenida){
			$this->avenida = $avenida;
		}

		function setQuarteirao($quarteirao){
			$this->quarteirao = $quarteirao;
		}

		function setNumCasa($num_casa){
			$this->num_casa = $num_casa;
		}

		function getNacionalidade(){
			return $this->nacionalidade;
		}

		function getProvincia(){
			return $this->provincia;
		}

		function getBairro(){
			return $this->bairro;
		}

		function getAvenida(){
			return $this->avenida;
		}

		function getQuarteirao(){
			return $this->quarteirao;
		}

		function getNumCasa(){
			return $this->num_casa;
		}
	}

?>